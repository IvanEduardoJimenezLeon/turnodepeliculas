# Test Project for Ekatar

# Instructions

If you don't have installed Docksal
Check the documentation : [Docksal](https://docs.docksal.io/getting-started/)

The database for the project is located in web/database/db-prueba.zip

# Commands to run project

`fin project start`
`fin ps` to check the server and port.

# Content

This project is compose by
- Welcome view
- Login
- Register
- Turnos Module (CRUD) 
- Peliculas module (CRUD)

