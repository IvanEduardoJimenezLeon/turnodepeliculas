<?php

namespace App\Http\Controllers;

use App\Models\Pelicula;
use App\Models\Turno;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
        if($request->ajax()){
            //return Pelicula::where('user_id', auth()->id())->get();
            $peliculas = Pelicula::where('user_id', auth()->id())->get();
            $turnos = Turno::where([
                ['user_id', auth()->id()],
                ['estado', '=', 1]
            ])->orderBy('turno', 'asc')->get();
            $data = [$peliculas, $turnos];
            return $data;
        }else{
            return view('peliculas');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file|image|max:2048'
        ]);

        $pelicula = new Pelicula();

        if($request->file()){
            $file_name = time().'_'.$request->file->getClientOriginalName();
            $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');


            $pelicula->nombre = $request->nombre;

            $pelicula->imagen_path = '/storage/app/public/' . $file_path;

            $pelicula->fecha_publicacion = $request->fecha_publicacion;
            $pelicula->estado = $request->estado;
            $pelicula->turno_id = $request->turno_id;
            $pelicula->user_id = auth()->id();
            $pelicula->save();

            return $pelicula;
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*$request->validate([
            'file' => 'required|image|max:2048'
        ]);


        if($request->file()) {
            $file_name = time() . '_' . $request->file->getClientOriginalName();
            $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');
            $pelicula->imagen_path = '/storage/app/public/' . $file_path;
        }*/
            $pelicula = Pelicula::find($id);
            $pelicula->nombre = $request->nombre;

            $pelicula->fecha_publicacion = $request->fecha_publicacion;
            $pelicula->estado = $request->estado;
            $pelicula->turno_id = $request->turno_id;
            $pelicula->user_id = auth()->id();
            $pelicula->save();

            return $pelicula;


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelicula = Pelicula::find($id);
        $pelicula->delete();
    }
}
