@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <sidebar/>
            </div>
            <div class="col-md-9">
                <h3 class="">{{ __('Dashboard') }}</h3>
                <div class="card">


                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <h5>{{ __('Bienvenido a Turnosdepeliculas!') }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
