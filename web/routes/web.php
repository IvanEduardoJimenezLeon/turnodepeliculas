<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Route for Turnos
use App\Http\Controllers\TurnoController;
Route::resource('turnos', TurnoController::class)->middleware('auth');

//Route for Peliculas
use App\Http\Controllers\PeliculaController;
Route::resource('peliculas', PeliculaController::class)->middleware('auth');
