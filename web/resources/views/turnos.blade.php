@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <sidebar />
            </div>
            <div class="col-md-9">
                <h3 class="">Modulo de Turnos</h3>
                <div class="card">

                    <div class="card-body">
                        <turnos />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
